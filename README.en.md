# nodelet_core

#### Description
Nodelet Core Metapackage

New in Electric: the nodelet packages are now part of nodelet_core. In previous releases, they were part of common.

For documentation on using nodelets, please see the nodelet package. For nodelet libraries similar in function to the topic_tools package, see nodelet_topic_tools.

#### Software Architecture
Software architecture description

nodelet_core

input:
```
nodelet_core/
├── nodelet
│   ├── CHANGELOG.rst
│   ├── CMakeLists.txt
│   ├── include
│   ├── mainpage.dox
│   ├── package.xml
│   ├── scripts
│   ├── src
│   └── srv
├── nodelet_core
│   ├── CHANGELOG.rst
│   ├── CMakeLists.txt
│   └── package.xml
└── nodelet_topic_tools
    ├── cfg
    ├── CHANGELOG.rst
    ├── CMakeLists.txt
    ├── include
    ├── mainpage.dox
    └── package.xml

```


#### Installation

1.  Download RPM

aarch64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_aarch64/aarch64/ros-noetic-ros-nodelet_core/ros-noetic-ros-nodelet_core-1.10.1-1.oe2203.aarch64.rpm

x86_64:

wget https://117.78.1.88/build/home:davidhan:branches:openEuler:22.03:LTS/standard_x86_64/x86_64/ros-noetic-ros-nodelet_core/ros-noetic-ros-nodelet_core-1.10.1-1.oe2203.x86_64.rpm 

2.  Install RPM

aarch64:

sudo rpm -ivh ros-noetic-ros-nodelet_core-1.10.1-1.oe2203.aarch64.rpm --nodeps --force

x86_64:

sudo rpm -ivh ros-noetic-ros-nodelet_core-1.10.1-1.oe2203.x86_64.rpm --nodeps --force

#### Instructions

Dependence installation

sh /opt/ros/noetic/install_dependence.sh

Exit the following output file under the /opt/ros/noetic/ directory , Prove that the software installation is successful

```
nodelet
├── cmake.lock
├── env.sh
├── include
│   └── nodelet
│       ├── NodeletList.h
│       ├── NodeletListRequest.h
│       ├── NodeletListResponse.h
│       ├── NodeletLoad.h
│       ├── NodeletLoadRequest.h
│       ├── NodeletLoadResponse.h
│       ├── NodeletUnload.h
│       ├── NodeletUnloadRequest.h
│       └── NodeletUnloadResponse.h
├── lib
│   ├── libnodeletlib.so
│   ├── nodelet
│   │   ├── cmake.lock
│   │   ├── declared_nodelets
│   │   ├── list_nodelets
│   │   └── nodelet
│   ├── pkgconfig
│   │   └── nodelet.pc
│   └── python2.7
│       └── dist-packages
├── local_setup.bash
├── local_setup.sh
├── local_setup.zsh
├── setup.bash
├── setup.sh
├── _setup_util.py
├── setup.zsh
└── share
    ├── common-lisp
    │   └── ros
    ├── gennodejs
    │   └── ros
    ├── nodelet
    │   └── cmake
    └── roseus
        └── ros
nodelet_core
├── cmake.lock
├── env.sh
├── local_setup.bash
├── local_setup.sh
├── local_setup.zsh
├── setup.bash
├── setup.sh
├── _setup_util.py
└── setup.zsh
nodelet_topic_tools
├── cmake.lock
├── env.sh
├── include
│   └── nodelet_topic_tools
│       └── NodeletThrottleConfig.h
├── lib
│   ├── pkgconfig
│   │   └── nodelet_topic_tools.pc
│   └── python2.7
│       └── dist-packages
├── local_setup.bash
├── local_setup.sh
├── local_setup.zsh
├── setup.bash
├── setup.sh
├── _setup_util.py
├── setup.zsh
└── share
    └── nodelet_topic_tools
        ├── cmake
        └── docs

```

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
